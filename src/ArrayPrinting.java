import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Simple class to define various array printing solutions where the order of the values does not matter.
 *
 * @author GHajba
 */
public class ArrayPrinting {

    private static int WARMUP_COUNT = 5;
    private static int BENCHMARK_COUNT = 5;

    private static final ConcurrentLinkedQueue<Integer> QUEUE = new ConcurrentLinkedQueue<>();

    public static void main(final String[] args) {

        if (args.length == 0) {
            return;
        }

        try {
            System.setErr(new PrintStream(MessageFormat.format("../runtimes_{0}.txt", args[0])));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ArrayPrinting arrayPrinting = new ArrayPrinting();
        arrayPrinting.doWork(args[0]);
    }

    private void complete_benchmark(final int[] array) {
        // System.err.println(LocalDateTime.now());
        long rc1 = printNormal(array);
        // System.err.println(LocalDateTime.now());
        long rc2 = printFirstAndLast(array);
        // System.err.println(LocalDateTime.now());
        long rc3 = printStream(array);
        // System.err.println(LocalDateTime.now());
        long rc4 = printParallelStream(array);
        // System.err.println(LocalDateTime.now());
        long rc5 = printDivided(array);
        // System.err.println(LocalDateTime.now());
        long rc6 = printForEach(array);
        // System.err.println(LocalDateTime.now());
        long rc7 = printThreads(array);
        // System.err.println(LocalDateTime.now());
        long rc8 = printFirstAndLast2(array);
        // System.err.println(LocalDateTime.now());
        long rc9 = printStreamJoiner(array);
        System.err.println(MessageFormat.format("Runtime for normal is {0}", rc1));
        System.err.println(MessageFormat.format("Runtime for first and last is {0}", rc2));
        System.err.println(MessageFormat.format("Runtime for stream is {0}", rc3));
        System.err.println(MessageFormat.format("Runtime for parallel stream is {0}", rc4));
        System.err.println(MessageFormat.format("Runtime for division is {0}", rc5));
        System.err.println(MessageFormat.format("Runtime for foreach is {0}", rc6));
        System.err.println(MessageFormat.format("Runtime for threads is {0}", rc7));
        System.err.println(MessageFormat.format("Runtime for first and last 2 is {0}", rc8));
        System.err.println(MessageFormat.format("Runtime for stream joiner is {0}", rc9));
        // System.err.println(LocalDateTime.now());
    }

    long printDivided(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        int part = array.length / 2;
        for (int i = 0; i < part; i++) {
            System.out.print(array[i]);
            System.out.print(array[part + i]);
        }
        if (array.length % 2 != 0) {
            System.out.print(array[array.length / 2 + 1]);
        }
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printFirstAndLast(final int[] array) {

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0, j = array.length - 1; i < j && i != j; i++, j--) {
            System.out.print(array[i]);
            System.out.print(array[j]);
        }
        if (array.length % 2 != 0) {
            System.out.print(array[array.length / 2 + 1]);
        }
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);

    }

    long printFirstAndLast2(final int[] array) {

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0, j = array.length - 1; i < j && i != j; i++, j--) {
            System.out.print(array[i] + "" + array[j]);
        }
        if (array.length % 2 != 0) {
            System.out.print(array[array.length / 2 + 1]);
        }
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);

    }

    long printForEach(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        for (int i : array) {
            System.out.print(array[i]);
        }
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printNormal(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printParallelStream(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        IntStream.of(array).parallel().forEach(System.out::print);
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printStream(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        IntStream.of(array).forEach(System.out::print);
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printStreamJoiner(final int[] array) {
        LocalDateTime start = LocalDateTime.now();
        System.out.println(IntStream.of(array).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    long printThreads(final int[] array) {
        for (int a : array) {
            QUEUE.add(a);
        }
        LocalDateTime start = LocalDateTime.now();
        Thread t1 = new Thread(new Printer());
        Thread t2 = new Thread(new Printer());
        Thread t3 = new Thread(new Printer());
        Thread t4 = new Thread(new Printer());
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        try {
            t1.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
    }

    public void run(final Function<int[], Long> function, final int[] array) {

        System.err.println(MessageFormat.format("Starting run with {0} warmup runs and {1} benchmark runs at {2}.", WARMUP_COUNT,
                BENCHMARK_COUNT, LocalDateTime.now()));
        System.err.flush();
        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < WARMUP_COUNT; i++) {
            function.apply(array);
        }
        System.err.println(MessageFormat.format("Warmup done in {0} seconds", start.until(LocalDateTime.now(), ChronoUnit.SECONDS)));
        System.err.flush();
        List<Long> runtimes = new ArrayList<>();
        for (int i = 0; i < BENCHMARK_COUNT; i++) {
            runtimes.add(function.apply(array));
        }
        System.err.println(MessageFormat.format("Total runtime for {0} iterations is {1} millis, average runtime is {2} millis",
                BENCHMARK_COUNT, Long.toString(runtimes.stream().mapToLong(a -> a).sum()),
                Double.toString(runtimes.stream().mapToLong(a -> a).average().getAsDouble())));
        for (Long runtime : runtimes) {
            System.err.println(runtime);
        }
        // System.err.println(MessageFormat.format("Finished at {0}", LocalDateTime.now()));
    }

    public void runAll(final int[] array, final ToLongFunction<int[]>... functions) {

        System.err.println(MessageFormat.format("Starting run with {0} functions at {1}.", functions.length, LocalDateTime.now()));
        System.err.flush();
        for (ToLongFunction<int[]> function : functions) {
            System.err.println(function.applyAsLong(array));
            System.err.flush();
        }
    }

    void doWork(final String arg) {
        int[] array = createArray();

        switch (arg) {
        case "0":
            runAll(array, this::printNormal, this::printFirstAndLast, this::printStream, this::printParallelStream, this::printDivided,
                    this::printForEach, this::printThreads, this::printFirstAndLast2, this::printStreamJoiner);
            break;
        case "1":
            run(this::printNormal, array);
            break;
        case "2":
            run(this::printFirstAndLast, array);
            break;
        case "3":
            run(this::printStream, array);
            break;
        case "4":
            run(this::printParallelStream, array);
            break;
        case "5":
            run(this::printDivided, array);
            break;
        case "6":
            run(this::printForEach, array);
            break;
        case "7":
            run(this::printThreads, array);
            break;
        case "8":
            run(this::printFirstAndLast2, array);
            break;
        case "9":
            run(this::printStreamJoiner, array);
            break;
        default:
            complete_benchmark(array);
            return;
        }
    }

    int[] createArray() {
        int array_size = 1_000_000;
        int[] array = new int[array_size];
        for (int i = 0; i < array_size; i++) {
            array[i] = i;
        }
        return array;
    }

    class Printer implements Runnable {

        @Override
        public void run() {
            Integer a = null;
            while ((a = QUEUE.poll()) != null) {
                System.out.print(a);
            }
        }
    }
}
