import java.util.Scanner;
import java.util.StringTokenizer;

public class RabbitJumps {

    public static void main(final String[] args) {

        Scanner length = new Scanner(System.in);
        System.out.print("Enter n : \n");
        int n = length.nextInt();

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the elements seperated by spaces: ");
        String input = sc.nextLine();
        sc.close();
        length.close();

        StringTokenizer strToken = new StringTokenizer(input);

        // Reads in the numbers to the array
        int[] rocks = new int[n];

        for (int x = 0; x < n; x++) {
            rocks[x] = Integer.parseInt((String) strToken.nextElement());
        }

        System.out.println("Number of rabit jumps " + countJumps(rocks));
    }

    private static int countJumps(final int[] rocks) {

        int jumps = 0;
        int i = 0, position = -1, distance = 0;
        int n = rocks.length;
        while (position < n - 1) {
            while (i < n && rocks[i] - distance <= 50) {
                i++;
            }
            if (i - 1 == position) {
                jumps = -1;
                position = n - 1;
            } else {
                position = i - 1;
                distance = rocks[position];
                jumps++;
            }
        }
        return jumps;
    }
}
